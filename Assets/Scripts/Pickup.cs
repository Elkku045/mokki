using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{

    [SerializeField] Transform hand;
    public GameObject curObject;
    public bool holding = false;
    RaycastHit hit;
    private Vector3 puldir;

    /*public InventoryItemData referenceItem;
    public GameObject Inventory;
    private bool InventoryOn;*/

    void Update()
    {
        /*if (Input.GetButtonDown("Inventory"))
        {
            if(InventoryOn == false)
            {
                Inventory.SetActive(true);
                InventoryOn = true;
            }
            else
            {
                Inventory.SetActive(false);
                InventoryOn = false;
            }

        }*/
        if (holding == true)
        {
            curObject.GetComponent<Rigidbody>().drag = 20;
            if (Vector3.Distance(curObject.transform.position, hand.position) > 0.01f)
            {
                Vector3 puldir = hand.position - curObject.transform.position;
                curObject.GetComponent<Rigidbody>().AddForce(puldir * 200);
            }
            curObject.transform.rotation = hand.rotation;
        }
        else
        {
            if (curObject != null)
            {
                curObject.GetComponent<Rigidbody>().drag = 1;
                curObject.GetComponent<Rigidbody>().useGravity = true;
                curObject = null;
            }
        }
        if (Physics.Raycast(gameObject.transform.position, transform.forward, out hit, 3f, 3))
        {
            // pick up an object
            if (Input.GetButtonDown("Interact") && hit.collider.gameObject.tag == "pickup" && holding == false)
            {
                curObject = hit.collider.gameObject;
                curObject.GetComponent<Rigidbody>().useGravity = false;
                holding = true;
            }
            // interact with a held object
            else if (Input.GetButtonDown("Fire1") && holding == true)
            {
                curObject.GetComponent<Rigidbody>().AddForce(transform.forward * 7f, ForceMode.Impulse);
                holding = false;
            }
            else if (Input.GetButtonDown("Interact") && holding == true)
            {
                holding = false;
            }
            else if (Input.GetButtonDown("Fire2") && holding == true)
            {
                /*if(curObject != null)
                {
                    referenceItem = curObject.GetComponent<ItemController>().Item;
                    Debug.Log(curObject.name + " added to inventory");
                    InventorySystem.current.Add(referenceItem);
                    Destroy(curObject);
                    holding = false;
                }*/
            }
        }
    }
}
