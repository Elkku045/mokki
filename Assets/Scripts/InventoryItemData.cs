using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory")]
public class InventoryItemData : ScriptableObject
{
    public string id;
    public string name;
    public Sprite icon;
    public GameObject item;
}
