using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveCC : MonoBehaviour
{
    private CharacterController controller;
    private CapsuleCollider col;

    public Animator camAnim;

    private float speed = 3f;
    private float gravity = -9.81f;
    private float groundDistance = 0.2f;

    [SerializeField] Transform groundCheck;
    [SerializeField] Transform CeilingCheck;
    [SerializeField] LayerMask groundMask;
    
    private Vector3 velocity;

    private bool isGrounded;
    private bool isCrouched = false;
    private bool isRunnig = false;
    /*private bool canStand = false;
    private bool isMooving = false;*/
    [SerializeField] Pickup Pickup;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        col = GetComponent<CapsuleCollider>();
    }

    void Update()
    {
        //liikkuminen
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;
        controller.Move(move * speed * Time.deltaTime);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        //putoaminen
        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
        //hyppy

        if (Input.GetButtonDown("Jump" ) && isGrounded)
        {
            velocity.y = Mathf.Sqrt(1.5f * -2 * gravity);
        }
        //k�vely animaatioita
        /*if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
        {
            isMooving = true;
        }
        else
        {
            isMooving = false;
        }
        if (isMooving == true && isRunnig == false)
        {

            camAnim.SetBool("walking", true);
            camAnim.SetBool("idle", false);
        }
        else
        {
            camAnim.SetBool("idle", true);
            camAnim.SetBool("walking", false);
        }*/

        //kyykky
        //canStand = Physics.CheckSphere(CeilingCheck.position, groundDistance, groundMask);
        if (Input.GetButton("Croutch")) /*|| canStand == true*/
        {
            isCrouched = true;
        }
        else
        {
            isCrouched = false;
        }
        if (isCrouched == true)
        {
            controller.height = 1f;
        }
        else
        {
            controller.height = 2f;
        }

        //juoksu
        if (Input.GetKey(KeyCode.LeftShift) && isCrouched == false)
        {
            isRunnig = true;
        }
        else
        {
            isRunnig = false;
        }
        if (isRunnig == true)
        {
            if (speed < 5)
            {
                speed += 0.1f;
            }
            
            /*juoksu animaatio
            camAnim.SetBool("running", true);
            camAnim.SetBool("idle", false);
            camAnim.SetBool("walking", false);*/
        }
        else
        {
            speed = 3f;
            //camAnim.SetBool("running", false);
        }

    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "enemy")
        {
            Application.Quit();
        }

        if (Pickup.curObject != null)
        {
            if (col.gameObject == Pickup.curObject)
            {
                Pickup.holding = false;
            }
        }

    }

}