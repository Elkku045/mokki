using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySystem : MonoBehaviour
{
    private Dictionary<InventoryItemData, Item> ItemDictionary;
    public List<Item> inventory { get; private set; }
    public static InventorySystem current;

    private void Awake()
    {
        inventory = new List<Item>();
        
        ItemDictionary = new Dictionary<InventoryItemData, Item>();
        current = this;
    }
    public void Add(InventoryItemData referenceData)
    {
        if (ItemDictionary.TryGetValue(referenceData, out Item value))
        {
            Debug.Log("add to stack");
            value.AddToStack();
        }
        else
        {
            Debug.Log("new item");
            Item newItem = new Item(referenceData);
            inventory.Add(newItem);
            ItemDictionary.Add(referenceData, newItem);
        }
    }

    public void Remove(InventoryItemData referenceData)
    {
        if (ItemDictionary.TryGetValue(referenceData, out Item value))
        {
            value.RemoveFromStack();

            if(value.stackSize == 0)
            {
                inventory.Remove(value);
                ItemDictionary.Remove(referenceData);
            }
        }
    }

    public class Item
    {
        public InventoryItemData data { get; private set; }
        public int stackSize { get; private set; }

        public Item(InventoryItemData source)
        {
            data = source;
            AddToStack();
        }

        public void AddToStack()
        {
            stackSize++;
        }

        public void RemoveFromStack()
        {
            stackSize--;
        }
    }
}
