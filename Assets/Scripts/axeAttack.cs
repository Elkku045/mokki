using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class axeAttack : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] BoxCollider collider;
    [SerializeField] Pickup Pickup;
    private bool canAttack = true;
    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1") && canAttack == true && Pickup.holding == false)
        {
            canAttack = false;
            collider.enabled = true;
            animator.SetTrigger("Attack");
            StartCoroutine(ResetAttack());
        }
        IEnumerator ResetAttack()
        {
            yield return new WaitForSeconds(0.7f);
            collider.enabled = false;
            yield return new WaitForSeconds(1);
            canAttack = true;
        }
    }
}
