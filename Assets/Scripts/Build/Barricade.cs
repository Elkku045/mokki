using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barricade : MonoBehaviour
{
    public bool barricadeUp = false;

    void Update()
    {
        if (barricadeUp == true)
        {
            gameObject.layer = 7;
            gameObject.GetComponent<MeshRenderer>().enabled = true;
        }
        else
        {
            gameObject.layer = 0;
            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
    }
}
