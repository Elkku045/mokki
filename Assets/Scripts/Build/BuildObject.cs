using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildObject : MonoBehaviour
{
    [SerializeField] GameObject item;
    [SerializeField] GameObject itemSpawn;
    private GameObject barricade;
    private RaycastHit hit;
    [SerializeField] Material CanPlace;
    [SerializeField] Material CantPlace;
    public bool objectblocking = true;
    private bool canBuild;
    private float posY;
    private Barricade BarricadeScript;
    private Quaternion rotation;

    void Update()
    {
        if (Physics.Raycast(gameObject.transform.position, transform.forward, out hit, 50f,3 & 7))
        {
            if (Input.GetButton("Rotate"))
            {
                posY = 1;
            }
            else
            {
                posY = 0;
            }
            item.transform.Rotate(0f, 0f, posY, Space.Self);
            item.transform.position = hit.point;
            if (objectblocking == true)
            {
                canBuild = false;
            }
            else if (Vector3.Distance(gameObject.transform.position,hit.point) < 5f && Vector3.Distance(gameObject.transform.position, hit.point) > 2.5f)
            {
                canBuild = true;
            }
            else 
            {
                canBuild = false;
            }
            if (Input.GetButtonDown("Fire1") && hit.collider.gameObject.tag == "barricade")
            {
                barricade = hit.collider.gameObject;
                Debug.Log(barricade.name);
                BarricadeScript = barricade.GetComponent<Barricade>();
                if (BarricadeScript.barricadeUp == false)
                {
                    BarricadeScript.barricadeUp = true;
                }
                else
                {
                    BarricadeScript.barricadeUp = false;
                }
            }
            else if (canBuild == true)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    Instantiate(itemSpawn, item.transform.position, item.transform.rotation);
                }
                item.GetComponent<MeshRenderer>().material = CanPlace;
            }
            else
            {
                item.GetComponent<MeshRenderer>().material = CantPlace;
            }
        }
        

    }
    
}
