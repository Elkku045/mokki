using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildCheck : MonoBehaviour
{
    [SerializeField] BuildObject BOS;
    void OnCollisionEnter(Collision col)
    {
        BOS.objectblocking = true;
    }
    void OnCollisionStay(Collision col)
    {
        BOS.objectblocking = true;
    }
    void OnCollisionExit(Collision col)
    {
        BOS.objectblocking = false;
    }
    
}
