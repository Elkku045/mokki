using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class EnemyAI : MonoBehaviour
{
    private NavMeshAgent agent;
    private Transform player;
    [SerializeField] LayerMask groundLayer, playerLayer;
    private Vector3 walkPoint;
    private bool walkPointIsSet;
    private EnemyLOS EnemyLOS;
    private Animator EnemyAnimator;
    [SerializeField] GameObject playerCam; 
    [SerializeField] Transform DeathCamPos;
    [SerializeField] Look lookScript;
    [SerializeField] MoveCC moveScript;
    void Awake()
    {
        EnemyAnimator = GetComponent<Animator>();
        player = GameObject.Find("Player").transform;
        agent = GetComponent<NavMeshAgent>();
        EnemyLOS = GameObject.Find("enemyEyes").GetComponent<EnemyLOS>();

    }

    void Update()
    {
        Vector3 playerDistance = transform.position -player.position;
        if(playerDistance.magnitude < 2f) Attack();
        else if(EnemyLOS.canSee == true) Rush();
        else Patroll();
        
    }

    private void Patroll()
    {
        agent.speed = 3.5f;
        if(!walkPointIsSet) SearchWalkPoint();

        if (walkPointIsSet)
            agent.SetDestination(walkPoint);

        Vector3 DTWP = transform.position -walkPoint;
        if (DTWP.magnitude < 2f)
            walkPointIsSet = false;
        EnemyAnimator.SetBool("IsRunnig", false);
    }

    private void SearchWalkPoint()
    {
        float randomZ = Random.Range(-40, 40);
        float randomX = Random.Range(-10, 10);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);
        if(Physics.Raycast(walkPoint, -transform.up, 2f, groundLayer))walkPointIsSet = true;

    }

    private void Rush()
    {
        agent.speed = 5f;
        walkPoint = new Vector3(player.position.x, player.position.y, player.position.z);
        agent.SetDestination(walkPoint);
        walkPointIsSet = true;
        EnemyAnimator.SetBool("IsRunnig", true);
    }

    private void Attack()
    {
        lookScript.enabled = false;
        moveScript.enabled = false;
        EnemyAnimator.SetTrigger("MikeAttack");
        playerCam.transform.position = Vector3.MoveTowards(playerCam.transform.position, DeathCamPos.position, Time.deltaTime * 20);
        playerCam.transform.LookAt(GameObject.Find("enemyEyes").transform);

    }
}