using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weaponHit : MonoBehaviour
{
    [SerializeField] float damage;
    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "enemy")
        {
            Debug.Log("attack");
            col.gameObject.GetComponent<health>().hp -= 25;
        }
    }
}
