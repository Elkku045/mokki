using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLOS : MonoBehaviour
{

    [SerializeField] float distance;
    [SerializeField] float angle;
    [SerializeField] float height;

    private int scanFerquancy = 30;
    [SerializeField] LayerMask targetLayers;
    [SerializeField] LayerMask occlusionLayers;
    public List<GameObject> Objects = new List<GameObject>();

    private Collider[] colliders = new Collider[50];
    private Mesh mesh;
    private Vector3[] vertices;
    private int[] triangles;
    private int count;
    private float scanInterval;
    private float scanTimer;
    public bool canSee = false;
    
    void Start ()
    {
        scanInterval = 1.0f / scanFerquancy;
    }
    void Update()
    {
        scanTimer -= Time.deltaTime;
        if (scanTimer < 0)
        {
            scanTimer += scanInterval;
            Scan();
        }
    }

    Mesh CreateWedgeMesh()
    {
        Mesh mesh = new Mesh();

        int numTriangles = 8;
        int numVertices = numTriangles * 3;

        vertices = new Vector3[]
        {
            //center
            new Vector3(0, 2.2f, 0),
            //right down
            Quaternion.Euler(0, -angle,0) * Vector3.forward * distance + -Vector3.up * height * 6,
            //left down
            Quaternion.Euler(0, angle,0) * Vector3.forward * distance + -Vector3.up * height * 6,
            //right up
            Quaternion.Euler(0, -angle,0) * Vector3.forward * distance + Vector3.up * height,
            //left up
            Quaternion.Euler(0, angle,0) * Vector3.forward * distance + Vector3.up * height,
        };
        triangles = new int []
        {
            2, 1, 0,
            0, 1, 3,
            0, 3, 4,
            0, 4, 2,
            3, 2, 4,
            3, 1, 2
        };

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
        return mesh;
    }

    private void OnValidate()
    {
        mesh = CreateWedgeMesh();
        scanInterval = 1.0f / scanFerquancy;
    }
    private void Scan()
    {
        count = Physics.OverlapSphereNonAlloc(transform.position, distance, colliders, targetLayers, QueryTriggerInteraction.Collide);

        Objects.Clear();
        for (int i = 0; i <count; ++i)
        {
            GameObject obj = colliders[i].gameObject;
            if (IsInSight(obj))
            {
                Objects.Add(obj);
                canSee = true;
            }
            else
            {
                canSee = false;
            }
        }
    }
    public bool IsInSight(GameObject obj)
    {
        Vector3 origin = transform.position;
        Vector3 dest = obj.transform.position;
        Vector3 direction = dest - origin;
        if (direction.y < height * -6 || direction.y > height)
        {
            return false;
        }
        direction.y = 0;
        float deltaAngle = Vector3.Angle(direction, transform.forward);
        if (deltaAngle > angle)
        {
            return false;
        }

        if (Physics.Linecast(origin, dest, occlusionLayers))
        {
            return false;
        }

        return true;
    }

}
